import requests
import urllib
import urllib.robotparser
from bs4 import BeautifulSoup


class Robot:

    def __init__(self, url):
        self.url = url
        self.robots_url = url + 'robots.txt'
        self.content = None
        self.rfp = None
        self.crawl_delay = None
        self.sitemap_content = None
        self.sitemap_urls = []
        self.parse_robots_txt()

    def parse_robots_txt(self):
        try:
            response = requests.get(self.robots_url)
            if response.ok:
                self.content = response.text

            if self.content:
                self.rfp = urllib.robotparser.RobotFileParser()
                self.rfp.set_url(self.robots_url)
                self.rfp.read()
                try:
                    self.crawl_delay = self.rfp.crawl_delay("*")
                except:
                    self.crawl_delay = 4

                # sitemap
                sitemap_url = ''
                for line in self.content.split("\n"):
                    if line.startswith('Sitemap'):
                        sitemap_url = line.split(': ')[1].split(' ')[0]
                        break

                if sitemap_url:
                    response = requests.get(sitemap_url)
                    self.sitemap_content = response.text
                    bs = BeautifulSoup(self.sitemap_content, features="xml")
                    loc_tags = bs.find_all("loc")
                    for tag in loc_tags:
                        self.sitemap_urls.append(tag.text)

        except requests.RequestException as e:
            return


    def can_fetch_url(self, url):
        can_fetch = self.rfp.can_fetch("*", url)
        if not can_fetch:
            print("Can not fetch", url)
        return can_fetch

