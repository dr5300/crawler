from psycopg2.pool import ThreadedConnectionPool
import threading
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
import urlcanon
from urllib.parse import urldefrag
import datetime
import requests
from link_model import Link
from page_data_model import PageData
from site_model import Site
from page_model import Page
from image_model import Image
from database import Database
import time
from bs4 import BeautifulSoup
import tldextract
from urllib.parse import urljoin
import re

robots_dict = {}  # key: domain, value: robot model
binary_data_allowed = True
images_allowed = True
allowed_domains = []
site_request_lock_dict = {}  # key: domain, value: boolean


def is_binary(response):
    binary_data_types = ['PDF', 'DOC', 'DOCX', 'PPT', 'PPTX']

    url_lower = response.url.lower()
    for type in binary_data_types:
        suffix = '.' + type.lower()
        if url_lower.endswith(suffix):
            return True

    content_type = response.headers.get('content-type').upper()
    for type in binary_data_types:
        if type in content_type:
            return True
    return False


def get_binary_data_type(response):
    binary_data_types = ['PDF', 'DOC', 'DOCX', 'PPT', 'PPTX']

    url_lower = response.url.lower()
    for type in binary_data_types:
        suffix = '.' + type.lower()
        if url_lower.endswith(suffix):
            return type

    content_type = response.headers.get('content-type').upper()
    for type in binary_data_types:
        if type in content_type:
            return type
    return None


def is_html(response):
    content_type = response.headers.get('content-type').upper()
    return 'HTML' in content_type


def is_in_allowed_domains(url):
    url_domain = get_domain_with_subdomain(url)
    for domain in allowed_domains:
        if domain in url_domain:
            return True
    return False


def get_domain_with_subdomain(url):
    ext = tldextract.extract(url)
    if ext.subdomain:
        return '.'.join((ext.subdomain, ext.domain, ext.suffix))
    else:
        return '.'.join((ext.domain, ext.suffix))


def href_url_check(url, database):
    return is_in_allowed_domains(url) \
           and database.get_page_from_db_by_url(url) is None \
           and 'javascript' not in url.lower() \
           and not (url.lower()).endswith('.jspx') \
           and not (url.lower()).endswith('.zip') \
           and not (url.lower()).endswith('.xlsx') \
           and not (url.lower()).endswith('.xls') \
           and not (url.lower()).endswith('.pps') \
           and not (url.lower()).endswith('.xsd') \
           and not (url.lower()).endswith('.txt') \
           and not (url.lower()).endswith('.xml') \
           and not (url.lower()).endswith('.swf') \
           and not (url.lower()).endswith('.ppsm') \
           and not (url.lower()).endswith('.mp4')


def image_url_check(url, site):
    return is_in_allowed_domains(url) \
           and (not robots_dict[site.domain].content or robots_dict[site.domain].can_fetch_url(url)) \
           and ((url.lower()).endswith('.jpg') or (url.lower()).endswith('.jpeg') or (url.lower()).endswith('.png'))


def parse_urls(base_url, soup, database, driver):
    urls = []
    # get href links
    for tag in soup.find_all(href=True):
        if tag.name != 'link':
            url = tag['href']
            urls.append(url)

    # get onclick links
    onclick_elements = driver.find_elements_by_xpath("//*[@onclick]")
    for element in onclick_elements:
        onclick_value = element.get_attribute("onclick")
        # single quotes
        single_quotes = re.findall(r'\'(.*?)\'', onclick_value)
        if single_quotes:
            url = single_quotes[0]
            urls.append(url)
        else:
            # double quotes
            double_quotes = re.findall(r'"(.*?)"', onclick_value)
            if double_quotes:
                url = double_quotes[0]
                urls.append(url)

    for url in urls:

        if url.startswith('www'):
            url = 'http://' + url

        url = urljoin(base_url, url)
        url = canonicalize(url)

        if href_url_check(url, database):
            domain = get_domain_with_subdomain(url)
            site = database.get_site_from_db_by_domain(domain)
            if site is None:
                site = Site(None, domain)
                site.parse_robot()
                robots_dict[domain] = site.robot
                database.add_site_to_db(site)
                site = database.get_site_from_db_by_domain(domain)
            site.robot = robots_dict[domain]
            if not site.robots_content or site.robot.can_fetch_url(url):
                page = Page(None, site.id, 'FRONTIER', url)
                database.add_page_to_db(page)


def parse_images(page, base_url, soup, database):
    urls = []
    # get src links
    for tag in soup.find_all('img'):
        url = tag['src']
        urls.append(url)

    for url in urls:
        if url.startswith('www'):
            url = 'http://' + url

        url = urljoin(base_url, url)
        url = canonicalize(url)
        site = database.get_site_from_db_by_id(page.site_id)

        if image_url_check(url, site):

            response = request_lock_get(url, site)
            if response is None:
                return

            accessed_time = datetime.datetime.now()
            if response.ok:
                # filename = url.split('/')[-1].split('.')[0]
                filename = url.split('/')[-1]
                content_type = response.headers.get('content-type')
                data = response.content
                image = Image(None, page.id, filename, content_type, data, accessed_time)
                database.add_image_to_db(image)


def parse_page(page, database, driver):
    soup = BeautifulSoup(page.html_content, features="html.parser")

    # get base url
    base_tag = soup.find('base', href=True)
    base_url = ""
    if base_tag:
        base_url = base_tag['href']
    else:
        base_url = page.url
    print("Base url", base_url)

    parse_urls(base_url, soup, database, driver)
    if images_allowed:
        parse_images(page, base_url, soup, database)


def request_lock_get(url, site):
    if site.domain in site_request_lock_dict:
        while True:
            if site_request_lock_dict[site.domain] == False:
                break

    site_request_lock_dict[site.domain] = True
    time.sleep(site.crawl_delay)
    try:
        response = requests.get(url, allow_redirects=True)
        site_request_lock_dict[site.domain] = False
        return response
    except requests.exceptions.RequestException as e:
        print(e)
        site_request_lock_dict[site.domain] = False
        return None


def selenium_lock_get(url, site, driver):
    if site.domain in site_request_lock_dict:
        while True:
            if site_request_lock_dict[site.domain] == False:
                break

    site_request_lock_dict[site.domain] = True
    time.sleep(site.crawl_delay)
    try:
        driver.get(url)
        site_request_lock_dict[site.domain] = False
        return driver.page_source
    except Exception as e:
        print(e)
        site_request_lock_dict[site.domain] = False
        return None


def fetch_and_render_page(page, driver, database):
    site = database.get_site_from_db_by_id(page.site_id)

    response = request_lock_get(page.url, site)
    if response is None:
        return

    accessed_time = datetime.datetime.now()
    http_status_code = response.status_code

    if is_html(response):
        html_content = selenium_lock_get(page.url, site, driver)
        if html_content is None:
            return

        accessed_time = datetime.datetime.now()
        duplicate_page = database.get_page_from_db_by_html_content(html_content)

        if duplicate_page:
            page.page_type_code = 'DUPLICATE'
            page.http_status_code = http_status_code
            page.accessed_time = accessed_time
            database.upadate_page_in_db(page)

            link = Link(page.id, duplicate_page.id)
            database.add_link_to_db(link)

        else:
            page.page_type_code = 'HTML'
            page.html_content = html_content
            page.http_status_code = http_status_code
            page.accessed_time = accessed_time
            database.upadate_page_in_db(page)

            parse_page(page, database, driver)

    elif is_binary(response):
        page.page_type_code = 'BINARY'
        page.http_status_code = http_status_code
        page.accessed_time = accessed_time
        database.upadate_page_in_db(page)

        if binary_data_allowed:
            data_type_code = get_binary_data_type(response)
            data = response.content
            page_data = PageData(None, page.id, data_type_code, data)
            database.add_page_data_to_db(page_data)

    else:
        print(page.url, 'IS OF TYPE', response.headers.get('content-type'))


def canonicalize(url):
    parsed_url = urlcanon.parse_url(url)
    urlcanon.whatwg(parsed_url)
    new_url = str(parsed_url)
    new_url = urldefrag(new_url)[0]
    return new_url


def get_webdriver():
    options = Options()
    options.add_argument("--incognito")
    options.add_argument("--ignore-certificate-errors")
    options.add_argument("--ignore-ssl-errors")
    options.add_argument("--headless")
    chrome_driver_path = 'C:/chromedriver/chromedriver.exe'

    driver = webdriver.Chrome(chrome_driver_path, options=options)
    return driver


def worker(lock, conn, worker_number):
    print("Worker", worker_number, "is starting")
    driver = get_webdriver()
    database = Database(lock, conn)

    has_page_in_frontier = True
    while True:
        page = database.get_next_page_in_frontier()
        if page:
            has_page_in_frontier = True
            fetch_and_render_page(page, driver, database)
        else:
            if not has_page_in_frontier:
                break
            else:
                has_page_in_frontier = False
                time.sleep(30)


def add_domains_sites_and_pages_to_db(domains, database):
    # add sites to database
    for domain in domains:
        site = Site(None, domain)
        if database.get_site_from_db_by_domain(domain) is None:
            site.parse_robot()
            robots_dict[domain] = site.robot
            database.add_site_to_db(site)

            # add pages to database
            site = database.get_site_from_db_by_domain(domain)
            site.robot = robots_dict[domain]
            if not site.robots_content or site.robot.can_fetch_url(site.root_url):
                page = Page(None, site.id, 'FRONTIER', site.root_url)
                database.add_page_to_db(page)
                if site.sitemap_content:
                    for url in robots_dict[domain].sitemap_urls:
                        if site.can_fetch_url(url):
                            page = Page(None, site.id, 'FRONTIER', canonicalize(url))
                            database.add_page_to_db(page)


if __name__ == '__main__':

    domains = ['evem.gov.si', 'e-prostor.gov.si']

    inital_domains = domains
    allowed_domains = domains
    binary_data_allowed = True
    images_allowed = True
    max_workers = 4

    tcp = ThreadedConnectionPool(1, max_workers, host="localhost", database="postgres", user="postgres", password="geslo12345")
    lock = threading.Lock()

    conn = tcp.getconn()
    database = Database(lock, conn)
    add_domains_sites_and_pages_to_db(inital_domains, database)
    tcp.putconn(conn)

    threads = []
    for i in range(max_workers):
        thread = threading.Thread(target=worker, args=(lock, tcp.getconn(), i))
        threads.append(thread)
        thread.start()



