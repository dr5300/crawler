from site_model import Site
from page_model import Page
from image_model import Image


class Database:

    def __init__(self, lock, conn):
        self.lock = lock
        self.conn = conn

    def get_next_page_in_frontier(self):
        # returns a page class from page record in database
        # sets page_type_code to PARSING
        with self.lock:
            cursor = self.conn.cursor()
            cursor.execute('SELECT * FROM crawldb.page WHERE page.page_type_code=%s ORDER BY page.id ASC', ('FRONTIER',))
            page_record = cursor.fetchone()
            self.conn.commit()
            cursor.close()
            if page_record:
                cursor = self.conn.cursor()
                id = page_record[0]
                cursor.execute('UPDATE crawldb.page SET page_type_code=%s WHERE page.id=%s', ('PARSING', id))
                self.conn.commit()
                cursor.close()
                return Page(id=page_record[0],
                            site_id=page_record[1],
                            page_type_code=page_record[2],
                            url=page_record[3],
                            html_content=page_record[4],
                            http_status_code=page_record[5],
                            accessed_time=page_record[6])
            else:
                return None


    def get_site_from_db_by_domain(self, domain):
        with self.lock:
            cursor = self.conn.cursor()
            cursor.execute('SELECT * FROM crawldb.site WHERE site.domain=%s', (domain,))
            site_record = cursor.fetchone()
            self.conn.commit()
            cursor.close()
            if site_record is None:
                return None
            else:
                return Site(id=site_record[0],
                            domain=site_record[1],
                            robots_content=site_record[2],
                            sitemap_content=site_record[3],
                            crawl_delay=site_record[4])


    def get_site_from_db_by_id(self, id):
        with self.lock:
            cursor = self.conn.cursor()
            cursor.execute('SELECT * FROM crawldb.site WHERE site.id=%s', (id,))
            site_record = cursor.fetchone()
            self.conn.commit()
            cursor.close()
            if site_record is None:
                return None
            else:
                return Site(id=site_record[0],
                            domain=site_record[1],
                            robots_content=site_record[2],
                            sitemap_content=site_record[3],
                            crawl_delay=site_record[4])


    def get_page_from_db_by_page_data(self, page_data):
        # use this only after get_page_from_db_by_url is unsucessful and
        # you know that page_type_code is BINARY
        with self.lock:
            cursor = self.conn.cursor()
            cursor.execute('SELECT * FROM crawldb.page_data WHERE page_data.data_type_code=%s AND page_data.data=%s', (page_data.data_type_code, page_data.data))
            page_data_record = cursor.fetchone()
            self.conn.commit()
            cursor.close()
            if page_data_record:
                cursor = self.conn.cursor()
                page_id = page_data_record[1]
                cursor.execute('SELECT * FROM crawldb.page WHERE page.id=%s', (page_id,))
                page_record = cursor.fetchone()
                self.conn.commit()
                cursor.close()
                return Page(id=page_record[0],
                            site_id=page_record[1],
                            page_type_code=page_record[2],
                            url=page_record[3],
                            html_content=page_record[4],
                            http_status_code=page_record[5],
                            accessed_time=page_record[6])
            else:
                return None


    def get_page_from_db_by_html_content(self, html_content):
        # use this only after get_page_from_db_by_url is unsucessful and
        # you know that page_type_code is HTML
        with self.lock:
            cursor = self.conn.cursor()
            cursor.execute('SELECT * FROM crawldb.page WHERE page.html_content=%s', (html_content,))
            page_record = cursor.fetchone()
            self.conn.commit()
            cursor.close()
            if page_record is None:
                return None
            else:
                return Page(id=page_record[0],
                            site_id=page_record[1],
                            page_type_code=page_record[2],
                            url=page_record[3],
                            html_content=page_record[4],
                            http_status_code=page_record[5],
                            accessed_time=page_record[6])

    def get_page_from_db_by_url(self, url):
        with self.lock:
            cursor = self.conn.cursor()
            cursor.execute('SELECT * FROM crawldb.page WHERE page.url=%s', (url,))
            page_record = cursor.fetchone()
            self.conn.commit()
            cursor.close()
            if page_record is None:
                return None
            else:
                return Page(id=page_record[0],
                            site_id=page_record[1],
                            page_type_code=page_record[2],
                            url=page_record[3],
                            html_content=page_record[4],
                            http_status_code=page_record[5],
                            accessed_time=page_record[6])


    def add_site_to_db(self, site):
        with self.lock:
            # check if site in db
            cursor = self.conn.cursor()
            cursor.execute('SELECT * FROM crawldb.site WHERE site.domain=%s', (site.domain,))
            site_record = cursor.fetchone()
            self.conn.commit()
            cursor.close()
            if site_record is None:
                cursor = self.conn.cursor()
                cursor.execute('INSERT INTO crawldb.site (domain, robots_content, sitemap_content, crawl_delay) VALUES (%s, %s, %s, %s)',
                               (site.domain, site.robots_content, site.sitemap_content, site.crawl_delay))
                self.conn.commit()
                cursor.close()
                print('Site', site.domain, 'added to db.')


    def add_page_to_db(self, page):
        with self.lock:
            # check if page in db
            cursor = self.conn.cursor()
            cursor.execute('SELECT * FROM crawldb.page WHERE page.url=%s', (page.url,))
            page_record = cursor.fetchone()
            self.conn.commit()
            cursor.close()
            if page_record is None:
                cursor = self.conn.cursor()
                cursor.execute('INSERT INTO crawldb.page (site_id, page_type_code, url, html_content, http_status_code, accessed_time) VALUES (%s, %s, %s, %s, %s, %s)',
                               (page.site_id, page.page_type_code, page.url, page.html_content, page.http_status_code, page.accessed_time))
                self.conn.commit()
                cursor.close()
                print('Page', page.url, 'added to db.')


    def add_link_to_db(self, link):
        with self.lock:
            cursor = self.conn.cursor()
            cursor.execute('INSERT INTO crawldb.link (from_page, to_page) VALUES (%s, %s)', (link.from_page, link.to_page))
            self.conn.commit()
            cursor.close()
            print('Link', link.from_page,  link.to_page, 'added to db.')


    def upadate_page_in_db(self, page):
        with self.lock:
            cursor = self.conn.cursor()
            cursor.execute('UPDATE crawldb.page SET page_type_code=%s, html_content=%s, http_status_code=%s, accessed_time=%s WHERE page.id=%s',
                           (page.page_type_code, page.html_content, page.http_status_code, page.accessed_time, page.id))
            self.conn.commit()
            cursor.close()
            print('Page', page.url, 'updated in db.')


    def add_page_data_to_db(self, page_data):
        with self.lock:
            cursor = self.conn.cursor()
            cursor.execute('INSERT INTO crawldb.page_data (page_id, data_type_code, data) VALUES (%s, %s, %s)', (page_data.page_id, page_data.data_type_code, page_data.data))
            self.conn.commit()
            cursor.close()
            print('Page data', page_data.data_type_code, 'added to db.')


    def add_image_to_db(self, image):
        with self.lock:
            cursor = self.conn.cursor()
            cursor.execute('INSERT INTO crawldb.image (page_id, filename, content_type, data, accessed_time) VALUES (%s, %s, %s, %s, %s)', (image.page_id, image.filename, image.content_type, image.data, image.accessed_time))
            self.conn.commit()
            cursor.close()
            print('Image', image.filename, 'added to db.')


    def get_image_from_db_by_data(self, data):
        with self.lock:
            cursor = self.conn.cursor()
            cursor.execute('SELECT * FROM crawldb.image WHERE image.data=%s', (data,))
            page_record = cursor.fetchone()
            self.conn.commit()
            cursor.close()
            if page_record is None:
                return None
            else:
                return Image(id=page_record[0],
                             page_id=page_record[1],
                             filename=page_record[2],
                             content_type=page_record[3],
                             data=page_record[4],
                             accessed_time=page_record[5])


    def upadate_parsing_to_frontier_pages(self):
        with self.lock:
            cursor = self.conn.cursor()
            cursor.execute('UPDATE crawldb.page SET page_type_code=%s WHERE page.page_type_code=%s', ('FRONTIER', 'PARSING'))
            self.conn.commit()
            cursor.close()


    def get_all_site_domains(self):
        with self.lock:
            cursor = self.conn.cursor()
            cursor.execute('SELECT (domain) FROM crawldb.site')
            sites_records = cursor.fetchall()
            self.conn.commit()
            cursor.close()
            domains = [tup[0] for tup in sites_records]
            return domains



