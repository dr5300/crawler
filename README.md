### Programming assignment 1 ###


## Installation

Use *pip* to install required packages:

```bash
pip install -r requirements.txt
```

## How to use?

1. Set parameters in the main function of *crawler.py* script. Parameters:
    - *initial_domains*: is a list of domains for which we get sites and pages that we add to frontier before crawling begins
    - *allowed_domains*: is a list of domains that we allow crawler to crawl
    - *binary_data_allowed*: is a Boolean, which defines whether a crawler should parse binary files and store them in to the database
    - *images_allowed*: is a Boolean, which defines whether a crawler should parse images from pages and store them in to the database
    - *max_workers*: maximum number of workers that crawler can use during crawling

2. Start crawler by runing the *crawler.py* script.


### Contributors ###
* **Denis Rajković**
* **Matic Bizjak**