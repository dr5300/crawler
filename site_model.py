import requests
from robot_model import Robot


class Site:

    def __init__(self, id, domain, robots_content=None, sitemap_content=None, crawl_delay=None):
        self.id = id
        self.domain = domain
        self.robots_content = robots_content
        self.sitemap_content = sitemap_content
        self.crawl_delay = crawl_delay
        self.root_url = 'http://' + self.domain + '/'
        self.robot = None


    def parse_robot(self):
        self.robot = Robot(self.root_url)
        self.robots_content = self.robot.content
        self.sitemap_content = self.robot.sitemap_content
        if self.robot.crawl_delay is None:
            self.crawl_delay = 4
        else:
            self.crawl_delay = self.robot.crawl_delay


    def can_fetch_url(self, url):
        return self.robot.rfp.can_fetch("*", url)
